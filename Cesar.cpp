#include<iostream>
#include<fstream>
#define MAX_LETTER_BLines 250
#include<string.h>
using namespace std;

int decale_alpha(char c,int decalage){
    if ((c+decalage) > 90) {
        return 65 +((c+decalage)%91);
    }
    else{
        return c+decalage;
    }
}

void encrypt_cesar(FILE *f,char* dest,int decalage){
    char lignes[MAX_LETTER_BLines];
    char* ligne = lignes;
    char c;
    int i = 0;
    FILE* encrypted_file = fopen(dest,"w");
    if(encrypted_file == NULL){
        cout << "ERREUR D'OUVERTURE DE FICHIER" << endl;
        return;
    }
    while(fgets(ligne,MAX_LETTER_BLines,f)){
        int taille_ligne = strlen(ligne);
        while(i<taille_ligne){
           c = ligne[i];
           if((c >= 65 && c <= 90)){
               c = decale_alpha(c,decalage);
               fprintf(encrypted_file,"%c",c);
           }
           i++;
        }
        i=0;
    }
    cout <<"Le fichier a ete chiffre : "<< dest <<endl;
    fclose(encrypted_file);
}

